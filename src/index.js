import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';
import registerServiceWorker from './registerServiceWorker';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import './styles/core.scss';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
