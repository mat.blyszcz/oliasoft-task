import * as types from '../actions/actionTypes';

const INITIAL_STATE = {
  selectedStory: null,
  stories: [],
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_STORY_DETAILS_SUCCESS:
      return {
        selectedStory: action.payload,
        stories: [],
      };
    case types.FETCH_STORIES_SUCCESS:
      return {
        selectedStory: null,
        stories: action.payload,
      };
    case types.FETCH_STORY_DETAILS_FAILURE:
    case types.FETCH_STORIES_FAILURE:
      return INITIAL_STATE;
    default:
      return state;
  }
}
