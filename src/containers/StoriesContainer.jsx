import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Home from '../components/Home/Home';
import StoriesChart from '../components/StoriesChart/StoriesChart';
import Spinner from '../components/Spinner/Spinner';
import { fetchStories } from '../actions';

export const StoriesContainer = Base =>
  class extends PureComponent {
    componentDidMount() {
      this.props.fetchStories();
    }

    render() {
      if (!this.props.stories.length) {
        return <Spinner />;
      }

      return <Base stories={this.props.stories} />;
    }
  };

const mapStateToProps = state => ({
  stories: state.storiesDetails.stories,
});

const mapDispatchToProps = dispatch => ({
  fetchStories: () => dispatch(fetchStories()),
})

export const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(StoriesContainer(Home));
export const StoriesChartContainer = connect(mapStateToProps, mapDispatchToProps)(StoriesContainer(StoriesChart));
