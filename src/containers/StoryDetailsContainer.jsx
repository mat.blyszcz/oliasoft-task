import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Spinner from '../components/Spinner/Spinner';
import StoryDetails from '../components/StoryDetails/StoryDetails';
import { fetchStoryDetails } from '../actions';

const StoryDetailsContainer = Base =>
  class extends PureComponent {
    componentDidMount() {
      this.props.fetchStoryDetails(this.props.match.params.id);
    }

    render() {
      if (!this.props.selectedStory) {
        return <Spinner />;
      }

      return <Base story={this.props.selectedStory} />;
    }
  };

const mapStateToProps = state =>  ({
  selectedStory: state.storiesDetails.selectedStory,
})

const mapDispatchToProps = dispatch => ({
  fetchStoryDetails: id => dispatch(fetchStoryDetails(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StoryDetailsContainer(StoryDetails));
