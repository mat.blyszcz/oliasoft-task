import React, { Component } from 'react';
import thunk from 'redux-thunk';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from '../../reducers';
import { HomeContainer, StoriesChartContainer } from '../../containers/StoriesContainer';
import Navbar from '../Navbar/Navbar';
import StoryDetailsContainer from '../../containers/StoryDetailsContainer';
import '../../styles/general/_container.scss';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

class App extends Component {
  render() {
    return (
      <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
          <React.Fragment>
            <Navbar />
            <div className='container'>
              <Route exact path='/' component={HomeContainer} />
              <Route path='/chart' component={StoriesChartContainer} />
              <Route path='/details/:id' component={StoryDetailsContainer} />
            </div>
          </React.Fragment>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
