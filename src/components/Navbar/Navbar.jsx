import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.scss';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNavbar: false,
    };

    this.toggleNavbar = ::this.toggleNavbar;
  }

  toggleNavbar() {
    this.setState({
      showNavbar: !this.state.showNavbar,
    });
  }

  render() {
    return (
      <nav className={`navbar ${this.state.showNavbar ? 'navbar--show' : ''}`}>
        <div className='navbar__burger' onClick={this.toggleNavbar}>
          <div className='navbar__burger--bar1' />
          <div className='navbar__burger--bar2' />
          <div className='navbar__burger--bar3' />
        </div>
        <NavLink exact to='/' className='navbar__item' activeClassName='navbar--active'>
          <i className='fa fa-home' />
          <p>Home</p>
        </NavLink>
        <NavLink to='/chart' className='navbar__item' activeClassName='navbar--active'>
          <i className='fa fa-line-chart' />
          <p>Chart</p>
        </NavLink>
      </nav>
    );
  }
}

export default Navbar;
