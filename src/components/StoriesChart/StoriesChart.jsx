import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ChartWrapper from '../ChartWrapper/ChartWrapper';
import Box from '../Box/Box';

class StoriesChart extends PureComponent {
  constructor(props) {
    super(props);

    this.getShuffledChartData = ::this.getShuffledChartData;
  }

  getShuffledChartData() {
    const scores = this.props.stories.map(story => story.score);
    scores.sort(() => 0.5 - Math.random());
    return scores.map((score, i) => ({ x: i, y: score }));
  }

  render() {
    return (
      <Box title='Stories score (randomized positions)'>
        <ChartWrapper data={this.getShuffledChartData()} />
      </Box>
    );
  }
}

StoriesChart.propTypes = {
  stories: PropTypes.array.isRequired,
}

export default StoriesChart;
