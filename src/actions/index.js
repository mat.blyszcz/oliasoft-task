import axios from 'axios';
import * as types from './actionTypes';

const ROOT_URL = 'https://hacker-news.firebaseio.com/v0';

function fetchStoriesSuccess(payload) {
  return {
    type: types.FETCH_STORIES_SUCCESS,
    payload,
  };
}

function fetchStoriesFailure(payload) {
  return {
    type: types.FETCH_STORIES_FAILURE,
    payload,
  };
}

function fetchStoriesDataFromIds(response) {
  const storiesId = response.data.slice(0, 10);
  const promises = storiesId.map(id => axios.get(`${ROOT_URL}/item/${id}.json`));

  return axios
    .all(promises)
    .then(res => res.map(res => res.data))
    .then(stories => stories.sort((a, b) => a.score < b.score));
}

export function fetchStories() {
  return dispatch =>
    axios
      .get(`${ROOT_URL}/topstories.json`)
      .then(fetchStoriesDataFromIds)
      .then(res => dispatch(fetchStoriesSuccess(res)))
      .catch(err => dispatch(fetchStoriesFailure(err)));
}

function fetchStoryDetailsSuccess(payload) {
  return {
    type: types.FETCH_STORY_DETAILS_SUCCESS,
    payload,
  };
}

function fetchStoryDetailsFailure(payload) {
  return {
    type: types.FETCH_STORIES_FAILURE,
    payload,
  };
}

function fetchStoryComments(response) {
  const story = response.data;
  const commentsIds = story.kids.slice(0, 10);
  const promises = commentsIds.map((id) => axios.get(`${ROOT_URL}/item/${id}.json`));
  story.comments = [];

  return axios.all(promises).then(res => {
    res.forEach(res => story.comments.push(res.data));
    return story;
  });
}

export function fetchStoryDetails(id) {
  return dispatch =>
    axios
      .get(`${ROOT_URL}/item/${id}.json`)
      .then(fetchStoryComments)
      .then(res => dispatch(fetchStoryDetailsSuccess(res)))
      .catch(res => dispatch(fetchStoryDetailsFailure(res)));
}
